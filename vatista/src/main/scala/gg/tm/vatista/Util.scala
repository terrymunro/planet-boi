package gg.tm.vatista

object Util {
  lazy val fibs: Stream[BigInt] =
    BigInt(0) #:: BigInt(1) #:: fibs.zip(fibs.tail).map { case (n_2, n_1) => n_2 + n_1 }
}
