package gg.tm.vatista

import akka.actor.ActorSystem
import gg.tm.vatista.Store.{EdgeProperties, Message}
import pw.reee._

/** Vatista - Named after a character in Under-Night In-Birth EXE late[st]
  *
  * I am starting with a rudimentary Monte Carlo Tree Search strategy where I
  * limit the possible moves by imposing a few constraints (which may reduce the
  * strength of the AI but there are far too many possibilities otherwise)
  */
object Vatista extends App {
  final val system: ActorSystem = ActorSystem("Vatista")
  final val logActor = system.actorOf(Log.props, "logActor")
  final val storeActor = system.actorOf(Store.props, "storeActor")

  def handleTurn(gs: GameState): List[Order] = {
    storeActor ! Message.Init(setup(gs))

    ???
  }

  def setup(gs: GameState): Array[Array[EdgeProperties]] =
    gs.planets.map(s => gs.planets.map(d =>
      EdgeProperties(
        Point.distance(s.location, d.location),
        relativeValue(s, d))))
  val growthRateMultiplier = 10
  def relativeValue(src: Planet, dst: Planet): Int =
    (dst.growthRate * growthRateMultiplier) - (dst.growthRate * Point.distance(src.location, dst.location))

  PlanetWars.start(handleTurn)
}
