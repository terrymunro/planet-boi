package gg.tm.vatista

import akka.actor.{Actor, ActorLogging, Props}
import akka.event.Logging.LogLevel

class Log extends Actor with ActorLogging {
  import Log._
  override def receive: Receive = {
    case Message(lvl, msg)  => log.log(lvl, msg)
    case Debug(msg)         => log.debug(msg)
    case Warning(msg)       => log.warning(msg)
    case Error(msg)         => log.error(msg)
  }
}

object Log {
  def props: Props = Props[Log]

  final case class Message(level: LogLevel, message: String)
  final case class Debug(message: String)
  final case class Warning(message: String)
  final case class Error(message: String)
}

