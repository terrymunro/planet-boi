package gg.tm.vatista

import akka.actor.{ActorRef, FSM, Props}
import gg.tm.vatista.Store._
import gg.tm.vatista.Log.Error

class Store(logActor: ActorRef) extends FSM[State, Data] {
  startWith(State.Uninitialized, Data.Uninitialized)

  when(State.Uninitialized) {
    case Event(Message.Init(am), Data.Uninitialized) =>
    goto(State.Active) using Data.AdjacencyMatrix(am)
  }

  when(State.Active) {
    case Event(Message.GetMatrix, Data.AdjacencyMatrix(matrix)) =>
      sender ! Message.Matrix(matrix)
      stay

    case Event(Message.UpdateMatrix(am), d@Data.AdjacencyMatrix(_)) =>
      stay using d.copy(matrix = am)

    case Event(Message.UpdateEdge(s, d, p), data@Data.AdjacencyMatrix(am)) =>
      am(s).update(d, p)
      stay using data.copy(matrix = am)
  }

  whenUnhandled {
    case Event(e, s) =>
      logActor ! Error(s"Received unhandled request $e in state $stateName/$s")
      stay
  }

  initialize()
}

object Store {
  final def props: Props = Props[Store]
  final case class EdgeProperties(distance: Int, relativeValue: Int)

  sealed trait Message extends Product with Serializable
  object Message {
    // Received Event Messages
    final case class Init(adjMatrix: Array[Array[EdgeProperties]]) extends Message
    final case class UpdateMatrix(adjMatrix: Array[Array[EdgeProperties]]) extends Message
    final case class UpdateEdge(src: Int, dst: Int, props: EdgeProperties) extends Message
    final case object GetMatrix extends Message

    // Sent Event Messages
    final case class Matrix(adjMatrix: Array[Array[EdgeProperties]]) extends Message
  }

  sealed trait State extends Product with Serializable
  object State {
    final case object Uninitialized extends State
    final case object Active extends State
  }

  sealed trait Data extends Product with Serializable
  object Data {
    final case object Uninitialized extends Data
    final case class AdjacencyMatrix(matrix: Array[Array[EdgeProperties]]) extends Data
  }
}
