package gg.tm.vatista

import scala.util.Random

import pw.reee.{GameState, Order}

object MCTS {
  type Index        = Int
  type ExploreParam = Int
  type HashCode     = Int
  type Count        = Double
  type Value        = Double

  private final lazy val r: Random = new Random()
  final val epsilon: Value = 1e-6

  private final case class TreeNode(
    gameState: GameState,
    children: Map[Index, TreeNode],
    visits: Count,
    total: Value) {
    def isLeaf: Boolean = !children.hasDefiniteSize
  }
}
