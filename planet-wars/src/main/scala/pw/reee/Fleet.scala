package pw.reee

final case class Fleet(
  owner           : Owner,
  ships           : Int,
  source          : Planet,
  destination     : Planet,
  totalTripLength : Int,
  turnsRemaining  : Int
)
