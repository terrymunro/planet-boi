package pw.reee

import scala.math.hypot

final case class Point(x: Double, y: Double)

object Point {

  /**
    * Calculate the distance between two points
    */
  final def distance(src: Point, dst: Point): Int = {
    val dx: Double = (src.x - dst.x).abs
    val dy: Double = (src.y - dst.y).abs
    hypot(dx, dy).ceil.toInt
  }
}
