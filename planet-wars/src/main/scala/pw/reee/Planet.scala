package pw.reee

final case class Planet(
  id        : Int,
  owner     : Owner,
  ships     : Int,
  growthRate: Int,
  location  : Point
)
