package pw.reee

import scala.annotation.tailrec

import pw.reee.Owner.Me

final case class GameState(turn: Int, planets: Array[Planet], fleets: Array[Fleet]) extends Equals {
  override def toString: String = s"GameState($turn,${planets.mkString(",")}${fleets.mkString(",")})"
  override def equals(o: scala.Any): Boolean = o match {
    case GameState(t2, p2, f2) => turn == t2 && p2.sameElements(planets) && f2.sameElements(fleets)
    case _                     => false
  }
}

object GameState {
  implicit final class GameStateSyntax(private val self: GameState) extends AnyVal {

    /**
      * Sends an order to the game engine.
      *
      * An order is composed of a source planet number, a destination planet number and a number of
      * ships.
      *
      * A few things to keep in mind:
      *   + You can issue many orders per turn if you like.
      *   + The planets are numbered starting at zero, not one.
      *   + You must own the source planet.
      *       If you break this rule, the game engine kicks your bot out of the game instantly.
      *   + You can't move more ships than are currently on the source planet.
      *   + The ships will take a few turns to reach their destination.
      *       Travel is not instant. See the Point.distance() function for more info.
      */
    def issue(o: Order): Either[String, GameState] = {
      val Order(srcId, dstId, ships) = o
      val src = self.planets(srcId)
      val dst = self.planets(dstId)
      if (src.owner != Me)
        Left("Not your planet!")
      else if (ships > src.ships)
        Left("That planet does not have enough ships!")
      else if (ships < 1)
        Right(self)
      else {
        val n = Point.distance(src.location, dst.location)
        println(s"${src.id} ${dst.id} $ships")
        Right(
          self.copy(
            planets = self.planets.updated(src.id, src.copy(ships = src.ships - ships)),
            fleets = self.fleets :+ Fleet(Me, ships, src, dst, n, n)))
      }
    }

    @tailrec
    def runTurn(orders: List[Order]): GameState =
      orders match {
        case Nil =>
          println(PlanetWars.TurnDelimiter)
          self.incrTurn
        case o :: os =>
          val n = issue(o) match {
            case Right(gs) => gs
            case Left(msg) => error(msg); self
          }
          n.runTurn(os)
      }

    def |+|(that: GameState): GameState =
      self.copy(
        planets = that.planets ++ (self.planets diff that.planets),
        fleets = that.fleets ++ (self.fleets diff that.fleets))

    def incrTurn: GameState =
      self.copy(
        turn    = self.turn + 1,
        fleets  = Array.empty,
        planets = Array.empty
      )
  }
}
