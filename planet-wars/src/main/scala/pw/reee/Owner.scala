package pw.reee

sealed trait Owner extends Product with Serializable

object Owner {
  final case object Me      extends Owner
  final case object Neutral extends Owner
  final case object Enemy   extends Owner

  final val fromInt: Int => Owner = {
    case 0 => Neutral
    case 1 => Me
    case 2 => Enemy
    case _ => sys.error("No.")
  }
}
