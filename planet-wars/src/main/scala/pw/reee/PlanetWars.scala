package pw.reee

import scala.io.StdIn
import scala.Array.empty
import scala.util.matching.Regex

object PlanetWars {
  final val TurnDelimiter: String = "go"
  final val PlanetR: Regex        = "P\\s+(\\d+.?\\d*)\\s+(\\d+.?\\d*)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s*".r
  final val FleetR: Regex         = "F\\s+(\\d+) (\\d+)\\s+(\\d+)\\s+(\\d+) (\\d+)\\s+(\\d+)\\s*".r

  def start(handleTurn: GameState => List[Order]): Unit =
    try {
      debug("Game Start")
      var line = ""
      var gs = GameState(0, empty, empty)

      do {
        line = StdIn.readLine()
        if (line == TurnDelimiter)
          gs = gs.runTurn(handleTurn(gs))
        else
          gs = parse(gs, line)
      } while (line.nonEmpty)
      debug("Game Over")
    } catch { case ex: Throwable =>
      error(s"I had a moment.\nException: $ex")
    }

  def parse(gs: GameState, in: String): GameState = in match {
    case PlanetR(x, y, owner, ships, rate) =>
      gs.copy(
        planets = gs.planets :+
          Planet(gs.planets.length, Owner.fromInt(owner.toInt), ships.toInt, rate.toInt, Point(x.toDouble, y.toDouble)))

    case FleetR(owner, ships, src, dst, trip, turns) =>
      try {
        gs.copy(
          fleets = gs.fleets :+
            Fleet(
              Owner.fromInt(owner.toInt),
              ships.toInt,
              gs.planets(src.toInt),
              gs.planets(dst.toInt),
              trip.toInt,
              turns.toInt))
      } catch {
        case _: ArrayIndexOutOfBoundsException =>
          error(s"Input String: $in\nCurrent GameState planets: ${gs.planets.mkString(",")}")
          gs
      }
  }
}
