package pw

import scala.{Console => C}

package object reee {
  final def debug(msg: String): Unit = {
    C.err.println(s"${C.GREEN_B}DEBUG${C.RESET}: $msg\n")
    C.err.flush()
  }
  final def error(msg: String): Unit = {
    C.err.println(s"\n${C.RED_B}ERROR${C.RESET}: $msg\n\n")
    C.err.flush()
  }
}
