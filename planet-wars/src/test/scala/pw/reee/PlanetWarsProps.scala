package pw.reee

class PlanetWarsProps extends Props {
  def is = s2"""
Properties:
  Distances should be commutative (a -> b == b -> a)    $p1
                                                 """

  def p1 = prop { (a: Point, b: Point) =>
    val res1 = Point.distance(a, b).abs
    val res2 = Point.distance(b, a).abs

    (res1 must beTypedEqualTo(res2)) and
      (res2 must beTypedEqualTo(res1))
  }
}
