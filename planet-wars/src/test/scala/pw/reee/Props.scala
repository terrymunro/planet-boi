package pw.reee

import org.specs2
import org.scalacheck.{Arbitrary, Gen, Shrink}
import org.specs2.ScalaCheck
import org.specs2.scalacheck.Parameters
import org.specs2.matcher.MatchResultCombinators

abstract class Props extends specs2.Spec with ScalaCheck with MatchResultCombinators {
  implicit val params: Parameters = Parameters(minTestsOk = 10000).verbose
  implicit val pointArbitrary: Arbitrary[Point] =
    Arbitrary(
      Gen.sized { s =>
        val ds = s.toDouble
        for {
          x <- Gen.chooseNum(-ds, ds)
          y <- Gen.chooseNum(-ds, ds)
        } yield Point(x, y)
      }
    )
  implicit val pointShrink: Shrink[Point] = Shrink {
    case Point(x, y) =>
      Shrink.shrink(x).map(Point(_, y)) append
        Shrink.shrink(y).map(Point(x, _))
  }
}
