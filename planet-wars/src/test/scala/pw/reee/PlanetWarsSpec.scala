package pw.reee

import scala.Array.empty

import pw.reee.Owner.{Enemy, Me, Neutral}

class PlanetWarsSpec extends Spec {
  val gs = GameState(0, empty, empty)
  val p1 = Planet(0, Neutral, 119, 0, Point(11.6135908004, 11.6587374197))
  val p2 = Planet(1, Me, 100, 5, Point(1.2902863101, 9.04078582767))
  val p3 = Planet(2, Enemy, 100, 5, Point(21.9368952907, 14.2766890117))
  val f1 = Fleet(Enemy, 50, p3, p1, 3, 2)
  val f2 = Fleet(Me, 12, p2, p1, 3, 3)
  val f3 = Fleet(Enemy, 19, p3, p2, 5, 4)
  val f4 = Fleet(Me, 99, p2, p3, 5, 4)

  "Point" >> {
    "It should be able to calculate distance to another Point" >> {
      val a = Point(1, 23)
      val b = Point(38, 56)

      val res = Point.distance(a, b)
      res must beTypedEqualTo(50)
    }
  }

  "Parser" >> {
    "It should be able to parse a single planet" >> {
      "#" | "Input" || "Expected" | "Comment" |>
        0 ! "P 11.6135908004 11.6587374197 0 119 0" !! p1 ! "Neutral planet, with 119 ships" |
        1 ! "P 1.2902863101  9.04078582767 1 100 5" !! p2 ! "My planet with 100 ships" |
        2 ! "P 21.9368952907 14.2766890117 2 100 5" !! p3 ! "Enemy planet with 100 ships" |
        (
          (
            n,
            input,
            expectation,
            comment) =>
            comment ==> {
              val GameState(_, p, _) = PlanetWars.parse(gs, input)
              p must be size 1
              p.head must beEqualTo(expectation.copy(id = 0))
            })
    }

    "It should be able to parse a single fleet" >> {
      val gs2 = gs.copy(planets = Array(p1, p2, p3))
      "#" | "Input" || "Expected" | "Comment" |>
        1 ! "F 2 50 2 0 3 2" !! f1 ! "Enemy fleet, 50 ships headed to Neutral Planet" |
        2 ! "F 1 12 1 0 3 3" !! f2 ! "My fleet, 12 ships headed to Neutral Planet" |
        3 ! "F 2 19 2 1 5 4" !! f3 ! "Enemy fleet, 19 ships headed to My Planet" |
        4 ! "F 1 99 1 2 5 4" !! f4 ! "My fleet, 99 ships headed to Enemy Planet" |
        (
          (
            _,
            input,
            expect,
            comment) =>
            comment ==> {
              val GameState(_, _, f) = PlanetWars.parse(gs2, input)
              f must be size 1
              f.head must beEqualTo(expect)
            })
    }

    "It should be able to parse all planets and fleets" in {
      val input =
        """P 11.6135908004 11.6587374197 0 119 0
          |P 1.2902863101  9.04078582767 1 100 5
          |P 21.9368952907 14.2766890117 2 100 5
          |F 2 50 2 0 3 2
          |F 1 12 1 0 3 3
          |F 2 19 2 1 5 4
          |F 1 99 1 2 5 4
          |""".stripMargin

      val r =
        input
          .split("\n")
          .foldLeft(State.get[GameState])((s, in) =>
            for {
              cur <- s
              nxt <- PlanetWars.parse(in)
              r = cur |+| nxt
              _ <- State.put(r)
            } yield r)
          .runA(gs)

      r must beTypedEqualTo(GameState(0, Array(p1, p2, p3), Array(f1, f2, f3, f4)))
    }
  }
}
