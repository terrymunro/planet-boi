package pw.reee

import org.specs2.mutable
import org.specs2.matcher.{
  ExpectationsDescription,
  MatchResultCombinators,
  MustExpectations,
  TraversableMatchers,
  ValueChecks
}
import org.specs2.mutable.Tables

abstract class Spec
    extends mutable.Spec with ValueChecks with MustExpectations with ExpectationsDescription with MatchResultCombinators
    with TraversableMatchers with Tables
