import sbt._

object Dependencies {
  val NO_DEPS = Seq.empty[ModuleID]

  /* Compiler Plugins */
  lazy val bm4           = "com.olegpy"     %% "better-monadic-for" % "0.2.4"
  lazy val kindProjector = "org.spire-math" %% "kind-projector"     % "0.9.7"
  lazy val macroParadise = "org.scalamacros" %% "paradise" % "2.1.0" cross CrossVersion.full

  /* Parsing */
  lazy val atto = "org.tpolecat" %% "atto-core" % "0.6.2"

  /* FP */
  lazy val typelevel = Seq(
    "org.typelevel" %% "cats-core"     % "1.1.0",
    "org.typelevel" %% "cats-effect"   % "1.0.0-RC2",
    "org.typelevel" %% "cats-mtl-core" % "0.3.0",
    "org.typelevel" %% "mouse"         % "0.17"
  )
  lazy val monocle: Seq[ModuleID] =
    Seq("core", "macro", "state")
      .map(n => "com.github.julien-truffaut" %% s"monocle-$n" % "1.5.1-cats")

  /* Bayesian inference */
  lazy val rainier: Seq[ModuleID] =
    Seq("core", "cats")
      .map(n => "com.stripe" %% s"rainier-$n" % "0.1.3")
  /* For Science */
  lazy val akka: Seq[ModuleID] =
    Seq("actor", "remote", "cluster", "cluster-tools", "persistence")
      .map(n => "com.typesafe.akka" %% s"akka-$n" % "2.5.14")

  /* Projects */
  lazy val boiDeps: Seq[ModuleID] = Seq(atto) ++ typelevel ++ monocle

  lazy val planetWarsDeps: Seq[ModuleID] = NO_DEPS
  lazy val vatistaDeps: Seq[ModuleID] = akka
  lazy val schwiDeps: Seq[ModuleID] = NO_DEPS
  lazy val shiroDeps: Seq[ModuleID] = NO_DEPS

  /* Testing */
  lazy val specs2: Seq[ModuleID] =
    Seq("core", "matcher-extra", "scalacheck", "html", "junit")
      .map(n => "org.specs2" %% s"specs2-$n" % "4.3.2")
  lazy val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.14.0"
  lazy val testDeps: Seq[ModuleID] = (specs2 :+ scalaCheck).map(_ % Test)
}
