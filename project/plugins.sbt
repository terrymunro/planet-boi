addSbtPlugin("org.wartremover" % "sbt-wartremover"         % "2.3.1" )
addSbtPlugin("org.wartremover" % "sbt-wartremover-contrib" % "1.2.1" )
addSbtPlugin("com.eed3si9n"    % "sbt-assembly"            % "0.14.7")
addSbtPlugin("io.get-coursier" % "sbt-coursier"            % "1.0.3" )
