import Dependencies._

lazy val commonSettings = Seq(
  scalaVersion := "2.12.6",
  scalacOptions ++= Seq(
    "-deprecation",                      // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",                // Specify character encoding used by source files.
    "-explaintypes",                     // Explain type errors in more detail.
    "-feature",                          // Emit warning and location for usages of features that should be imported explicitly.
    "-language:existentials",            // Existential types (besides wildcard types) can be written and inferred
    "-language:experimental.macros",     // Allow macro definition (besides implementation and application)
    "-language:higherKinds",             // Allow higher-kinded types
    "-language:implicitConversions",     // Allow definition of implicit functions called views
    "-unchecked",                        // Enable additional warnings where generated code depends on assumptions.
    "-Xcheckinit",                       // Wrap field accessors to throw an exception on uninitialized access.
//    "-Xfatal-warnings",                  // Fail the compilation if there are any warnings.
    "-Xfuture",                          // Turn on future language features.
    "-Xlint:adapted-args",               // Warn if an argument list is modified to match the receiver.
    "-Xlint:by-name-right-associative",  // By-name parameter of right associative operator.
    "-Xlint:constant",                   // Evaluation of a constant arithmetic expression results in an error.
    "-Xlint:delayedinit-select",         // Selecting member of DelayedInit.
    "-Xlint:doc-detached",               // A Scaladoc comment appears to be detached from its element.
    "-Xlint:inaccessible",               // Warn about inaccessible types in method signatures.
    "-Xlint:infer-any",                  // Warn when a type argument is inferred to be `Any`.
    "-Xlint:missing-interpolator",       // A string literal appears to be missing an interpolator id.
    "-Xlint:nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Xlint:nullary-unit",               // Warn when nullary methods return Unit.
    "-Xlint:option-implicit",            // Option.apply used implicit view.
    "-Xlint:package-object-classes",     // Class or object defined in package object.
    "-Xlint:poly-implicit-overload",     // Parameterized overloaded implicit methods are not visible as view bounds.
    "-Xlint:private-shadow",             // A private field (or class parameter) shadows a superclass field.
    "-Xlint:stars-align",                // Pattern sequence wildcard must align with sequence component.
    "-Xlint:type-parameter-shadow",      // A local type parameter shadows a type already in scope.
    "-Xlint:unsound-match",              // Pattern match may not be typesafe.
    "-Yno-adapted-args",                 // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
    "-Ypartial-unification",             // Enable partial unification in type constructor inference
    "-Ywarn-dead-code",                  // Warn when dead code is identified.
    "-Ywarn-extra-implicit",             // Warn when more than one implicit parameter section is defined.
    "-Ywarn-inaccessible",               // Warn about inaccessible types in method signatures.
    "-Ywarn-infer-any",                  // Warn when a type argument is inferred to be `Any`.
    "-Ywarn-nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
    "-Ywarn-nullary-unit",               // Warn when nullary methods return Unit.
    "-Ywarn-numeric-widen",              // Warn when numerics are widened.
    "-Ywarn-unused:implicits",           // Warn if an implicit parameter is unused.
    "-Ywarn-unused:imports",             // Warn if an import selector is not referenced.
    "-Ywarn-unused:locals",              // Warn if a local definition is unused.
    "-Ywarn-unused:explicits",           // Warn if a value parameter is unused.
    "-Ywarn-unused:patvars",             // Warn if a variable bound in a pattern is unused.
    "-Ywarn-unused:privates",            // Warn if a private member is unused.
    "-Ywarn-value-discard",              // Warn when non-Unit expression results are unused.
  ),
  scalacOptions in Test --= Seq("-Ywarn-value-discard", "-Yno-predef"),
  scalacOptions in (Compile, console) --= Seq("-Ywarn-unused:imports", "-Xfatal-warnings"),
  test in assembly    := {},
  resolvers           += Resolver.sonatypeRepo("releases"),
  libraryDependencies ++= testDeps,
  autoCompilerPlugins := true,
  addCompilerPlugin(bm4),
  addCompilerPlugin(macroParadise)
)

lazy val planetBoi = (project in file("."))
  .aggregate(boi, vatista)
  .settings(Seq(scalaVersion := "2.12.6"))

lazy val nu = (project in file("nu"))
  .settings(
    commonSettings,
    version             := "0.1.0",
    name                := "nu")

lazy val boi = (project in file("boi"))
  .dependsOn(nu)
  .settings(
    commonSettings,
    organization        := "gg.tm",
    version             := "0.1.0",
    name                := "Boi",
    mainClass           := Some("gg.tm.boi.Boi"),
    exportJars          := true,
    libraryDependencies ++= boiDeps,
    assemblyOption in assembly  := (assemblyOption in assembly).value.copy(
      includeScala       = false,
      includeDependency  = false
    ),
    assemblyJarName in assembly := "boi.jar",
    wartremoverWarnings in (Compile, compile) ++= Warts.allBut(
      Wart.Nothing, Wart.PublicInference, Wart.Any),
    addCompilerPlugin(kindProjector)
  )

lazy val planetWars = (project in file("planet-wars"))
  .settings(
    commonSettings,
    organization        := "pw.reee",
    version             := "0.2.0",
    name                := "Planet-Wars",
    libraryDependencies ++= planetWarsDeps,
  )

lazy val vatista = (project in file("vatista"))
  .dependsOn(planetWars)
  .settings(
    commonSettings,
    organization        := "gg.tm",
    version             := "0.1.0",
    name                := "Vatista",
    libraryDependencies ++= vatistaDeps,
    mainClass           := Some("gg.tm.vatista.Vatista"),
    assemblyJarName in assembly := "vatista.jar",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(
      includeScala       = false,
      includeDependency  = false
    ),
  )

lazy val schwi = (project in file("schwi"))
  .dependsOn(planetWars)
  .settings(
    commonSettings,
    organization        := "gg.tm",
    version             := "0.1.0",
    name                := "Schwi",
    libraryDependencies ++= schwiDeps,
    mainClass           := Some("gg.tm.schwi.Schwi"),
    assemblyJarName in assembly := "schwi.jar",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(
      includeScala       = false,
      includeDependency  = false
    ),
  )

lazy val shiro = (project in file("shiro"))
  .dependsOn(planetWars)
  .settings(
    commonSettings,
    organization        := "gg.tm",
    version             := "0.1.0",
    name                := "Shiro",
    libraryDependencies ++= shiroDeps,
    mainClass           := Some("gg.tm.shiro.Shiro"),
    assemblyJarName in assembly := "shiro.jar",
    assemblyOption in assembly := (assemblyOption in assembly).value.copy(
      appendContentHash  = true,
      includeScala       = false,
      includeDependency  = false
    ),
  )
