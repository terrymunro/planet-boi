package gg.tm.boi.pw

import cats.implicits._
import cats.kernel.{Order => Ord}
import gg.tm.boi.pw.Owner.{Enemy, Me, Neutral}
import monocle.macros.Lenses

@Lenses final case class Fleet(
  owner          : Owner,
  ships          : Ships,
  source         : Planet,
  destination    : Planet,
  totalTripLength: Distance,
  turnsRemaining : Turn
) {
  def isMine   : Boolean = owner === Me
  def isEnemies: Boolean = owner === Enemy
  def isNeutral: Boolean = owner === Neutral

  def mightCapture: Boolean =
    ships > destination.ships + (destination.growthRate * turnsRemaining)
}

object Fleet {
  implicit val fleetOrdering: Ord[Fleet] =
    Ord.from[Fleet]((a, b) => a.ships.compare(b.ships))
  implicit val fleetEquiv: Equiv[Fleet] =
    Equiv.by[Fleet, (Owner, Ships, Planet, Planet)](f =>
      (f.owner, f.ships, f.source, f.destination))
}
