package gg.tm.boi.pw

import scala.math.hypot

import cats.Monoid
import monocle.macros.Lenses

@Lenses final case class Point(x: Double, y: Double)

object Point {
  /**
    * Calculate the distance between two points
    */
  final def distance(src: Point, dst: Point): Distance = {
    val dx: Double = (src.x - dst.x).abs
    val dy: Double = (src.y - dst.y).abs
    Distance(hypot(dx, dy).ceil.toInt)
  }

  implicit final class PointSyntax(private val self: Point) extends AnyVal {
    def distanceTo(that: Point): Distance =
      Distance(distance(self, that))

    def |/| (n: Int): Point = Point(self.x / n, self.y / n)
  }

  implicit val pointMonoid: Monoid[Point] = new Monoid[Point] {
    override def empty: Point = Point(0, 0)
    override def combine(a: Point, b: Point): Point =
      Point(a.x + b.x, a.y + b.y)
  }
}
