package gg.tm.boi.pw

import cats.Show
import monocle.macros.Lenses

@Lenses final case class Order(srcId: PlanetID, dstId: PlanetID, ships: Ships)

object Order {
  implicit val orderShow: Show[Order] =
    Show.show[Order](o => s"${o.srcId} ${o.dstId} ${o.ships}")
}
