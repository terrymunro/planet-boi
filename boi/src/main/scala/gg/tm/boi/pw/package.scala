package gg.tm.boi

import scala.{Console => C}
import scala.collection.SeqView

import cats.{Applicative, Eval, Foldable, Functor, Traverse, UnorderedTraverse}
import cats.implicits._
import cats.kernel.{Eq, Order => Ord}
import gg.tm.nu
import monocle.Iso

package object pw {
  type IndexedStream[A] = SeqView[A, IndexedSeq[A]]

  @nu type PlanetID = Int
  implicit val planetIdEq: Eq[PlanetID] = Eq.fromUniversalEquals[PlanetID]

  @nu type Ships = Int
  implicit val shipsOrder: Ord[Ships] = Ord.fromOrdering[Ships]
  implicit object shipsNumeric extends Numeric[Ships] {
    override def plus(x: Ships, y: Ships): Ships = Ships(x+y)
    override def minus(x: Ships, y: Ships): Ships = Ships(x-y)
    override def times(x: Ships, y: Ships): Ships = Ships(x*y)
    override def negate(x: Ships): Ships = Ships(-x)
    override def fromInt(x: Int): Ships = Ships(x)
    override def toInt(x: Ships): Int = x
    override def toLong(x: Ships): Long = x.toLong
    override def toFloat(x: Ships): Float = x.toFloat
    override def toDouble(x: Ships): Double = x.toDouble
    override def compare(x: Ships, y: Ships): Int = x.compare(y)
  }

  @nu type Turn = Int
  implicit final class TurnOps(private val self: Turn) extends AnyVal {
    def incr: Turn = Turn(self + 1)
    def max(that: Turn): Turn = Turn(math.max(self,that))
  }
  implicit val turnOrder: Ord[Turn] = Ord.from(_.compare(_))

  @nu type Distance = Int
  implicit val distanceOrder: Ord[Distance] = Ord.from(_.compare(_))
  implicit object distanceNumeric extends Numeric[Distance] {
    override def plus(x: Distance, y: Distance): Distance = Distance(x + y)
    override def minus(x: Distance, y: Distance): Distance = Distance(x - y)
    override def times(x: Distance, y: Distance): Distance = Distance(x * y)
    override def negate(x: Distance): Distance = Distance(-x)
    override def fromInt(x: Int): Distance = Distance(x)
    override def toInt(x: Distance): Int = x
    override def toLong(x: Distance): Long = x.toLong
    override def toFloat(x: Distance): Float = x.toFloat
    override def toDouble(x: Distance): Double = x.toDouble
    override def compare(x: Distance, y: Distance): Int = x.compare(y)
  }

  val turnDistIso: Iso[Turn, Distance] = Iso[Turn, Distance](Distance.apply)(Turn.apply)
  val intDistIso: Iso[Int, Distance] = Iso[Int, Distance](Distance.apply)(identity)
  val intTurn: Iso[Int, Turn] = intDistIso.composeIso(turnDistIso.reverse)
  val intShips: Iso[Int, Ships] = Iso[Int, Ships](Ships.apply)(identity)

  def debug(msg: String): Unit = {
    C.err.println(s"${C.GREEN_B}DEBUG${C.RESET}: $msg\n")
    C.err.flush()
  }
  def error(msg: String): Unit = {
    C.err.println(s"\n${C.RED_B}ERROR${C.RESET}: $msg\n\n")
    C.err.flush()
  }

  implicit object IndexedSeqInstances extends Functor[IndexedSeq] with Foldable[IndexedSeq] with Traverse[IndexedSeq] with UnorderedTraverse[IndexedSeq] {
    override def foldLeft[A, B](fa: IndexedSeq[A], b: B)(f: (B, A) => B): B = fa.foldLeft(b)(f)
    override def foldRight[A, B](fa: IndexedSeq[A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] =
      Eval.defer(fa.foldRight(lb)(f))
    override def traverse[G[_], A, B](fa: IndexedSeq[A])(f: A => G[B])(implicit A: Applicative[G]): G[IndexedSeq[B]] =
      fa.foldLeft(A.pure(IndexedSeq.empty[B]))((acc, a) => A.map2(acc, f(a))(_ :+ _))
  }
  implicit object IndexedStreamInstances extends Functor[IndexedStream] with Foldable[IndexedStream] with Traverse[IndexedStream] with UnorderedTraverse[IndexedStream] {
    override def foldLeft[A, B](fa: IndexedStream[A], b: B)(f: (B, A) => B): B = fa.foldLeft(b)(f)
    override def foldRight[A, B](fa: IndexedStream[A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] = Eval.defer(fa.foldRight(lb)(f))
    override def traverse[G[_], A, B](fa: IndexedStream[A])(f: A => G[B])(implicit A: Applicative[G]): G[IndexedStream[B]] =
      fa.foldLeft(A.pure(IndexedSeq.empty[B]))((acc, a) => A.map2(acc, f(a))(_ :+ _)).map(_.view)
  }

  def listToIndexedStream[A]: Iso[List[A], IndexedStream[A]] = Iso[List[A], IndexedStream[A]](_.toIndexedSeq.view)(_.toList)
  def indexedStreamToIndexedSeq[A]: Iso[IndexedStream[A], IndexedSeq[A]] = Iso[IndexedStream[A], IndexedSeq[A]](_.toIndexedSeq)(_.view)
}
