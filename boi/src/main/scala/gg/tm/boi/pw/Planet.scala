package gg.tm.boi.pw

import cats.Eq
import cats.implicits._
import gg.tm.boi.pw.Owner.{Enemy, Me, Neutral}
import monocle.Lens
import monocle.macros.Lenses

@Lenses final case class Planet(
  id        : PlanetID,
  owner     : Owner,
  ships     : Ships,
  growthRate: Int,
  location  : Point
)

object Planet {
  val shipsInt: Lens[Planet, Int] = ships.composeIso(intShips.reverse)

  implicit val planetEq: Eq[Planet] = Eq.fromUniversalEquals[Planet]
  implicit val planetEquiv: Equiv[Planet] = Equiv.by[Planet, PlanetID](_.id)

  implicit final class PlanetSyntax(private val self: Planet) extends AnyVal {
    def isMine: Boolean    = self.owner === Me
    def isNeutral: Boolean = self.owner === Neutral
    def isEnemies: Boolean = self.owner === Enemy

    def shipsNeededFrom(that: Planet): Int =
      1 + self.ships + (if (isNeutral) 0 else self.growthRate * self.distanceTo(that))

    def whoseCloser(gs: GameState): Owner = {
      val (mine, enemies) =
        gs.planets.view
          .filterNot(_.isNeutral)
          .partition(_.isMine)
      val (mineSum, enemySum) = (mine.map(_.ships).sum, enemies.map(_.ships).sum)
      if (mineSum > enemySum) Me
      else if (enemySum > mineSum) Enemy
      else Neutral
    }

    def distanceTo(that: Planet): Distance =
      self.location.distanceTo(that.location)
  }
}
