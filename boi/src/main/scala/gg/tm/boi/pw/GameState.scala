package gg.tm.boi.pw

import atto._
import atto.Atto._
import cats.implicits._
import gg.tm.boi.pw.Owner.{Enemy, Me, Neutral}
import monocle.macros.Lenses

@Lenses final case class GameState(turn: Turn, planets: IndexedSeq[Planet], fleets: IndexedSeq[Fleet])

object GameState {
  implicit final class GameStateSyntax(private val self: GameState) extends AnyVal {
    def winning: Owner = GameState.winning(self)

    def myFleets: IndexedStream[Fleet] = self.fleets.view.filter(_.isMine)
    def myPlanets: IndexedStream[Planet] = self.planets.view.filter(_.isMine)
    def myTotalShips: Ships = Ships(myPlanets.map(_.ships).sum + myFleets.map(_.ships).sum)
    def myCenter: Point = myPlanets.foldMap(_.location) |/| myPlanets.size

    def enemyFleets: IndexedStream[Fleet] = self.fleets.view.filter(_.isEnemies)
    def enemyPlanets: IndexedStream[Planet] = self.planets.view.filter(_.isEnemies)
    def enemyCenter: Point = enemyPlanets.foldMap(_.location) |/| enemyPlanets.size
    def enemyTotalShips: Ships = Ships(enemyPlanets.map(_.ships).sum + enemyFleets.map(_.ships).sum)

    def toPlanets(ids: IndexedSeq[PlanetID]): IndexedSeq[Planet] = ids.map(self.planets.apply)

    def possibleTargets(maxShips: Ships): IndexedStream[Planet] =
      self.planets.view.filterNot(_.isMine).filter(_.ships <= maxShips)

    /** Sends an order to the game engine. An order is composed of a source
      * planet number, a destination planet number, and a number of ships. A
      * few things to keep in mind:
      * you can issue many orders per turn if you like.
      * the planets are numbered starting at zero, not one.
      * you must own the source planet. If you break this rule, the game
      * engine kicks your bot out of the game instantly.
      * you can't move more ships than are currently on the source planet.
      * the ships will take a few turns to reach their destination. Travel
      * is not instant. See the Distance() function for more info.
      */
    def issue(o: Order): GameState = {
      val Order(srcId, dstId, ships) = o
      val src = self.planets(srcId)
      val dst = self.planets(dstId)
      if (src.owner =!= Me || ships > src.ships || ships < 1) self
      else {
        println(show"$o")
        val n = src.distanceTo(dst)
        val f = Fleet(Me, ships, src, dst, n, turnDistIso.reverseGet(n) )
        fleets.modify(_:+ f)
          .compose(planets.modify(_.updated(src.id, Planet.shipsInt.modify(_ - ships)(src))))(self)
      }
    }
  }

  final def initialState(turn: Turn): ParseResult[GameState] = parser.parse("").map(_(turn))
  final def winning(state: GameState): Owner = {
    val me = state.myTotalShips
    val enemy = state.enemyTotalShips

    if (me === enemy)    Neutral // Draw
    else if (me > enemy) Me
    else                 Enemy
  }

  final def intBetween(min: Int, max: Int): Parser[Int] =
    int.filter(x => min <= x && x <= max)
  final val positiveInt: Parser[Int] = int.filter(_ >= 0)
  final val ownerParser: Parser[Owner] = intBetween(0, 2).map(Owner.intIso.reverseGet)
  final val shipsParser: Parser[Ships] = positiveInt.map(intShips.get)
  final val distanceParser: Parser[Distance] = positiveInt.map(intDistIso.get)
  final val turnParser: Parser[Turn] = positiveInt.map(intTurn.get)
  final val pointParser: Parser[Point] =
    for {
      x <- double <~ skipWhitespace
      y <- double
    } yield Point(x, y)

  final val planetParser: Parser[PlanetID => Planet] =
    for {
      _     <- char('P')   <~ skipWhitespace
      pt    <- pointParser <~ skipWhitespace
      owner <- ownerParser <~ skipWhitespace
      ships <- shipsParser <~ skipWhitespace
      rate  <- positiveInt
    } yield (id: PlanetID) => Planet(id, owner, ships, rate, pt)

  final val fleetParser: IndexedSeq[Planet] => Parser[Fleet] =
    (planets: IndexedSeq[Planet]) =>
      for {
        _     <- char('F')   <~ skipWhitespace
        owner <- ownerParser <~ skipWhitespace
        ships <- shipsParser <~ skipWhitespace
        len = planets.length
        src   <- intBetween(0, len) <~ skipWhitespace
        dst   <- intBetween(0, len) <~ skipWhitespace
        trip  <- distanceParser <~ skipWhitespace
        turns <- turnParser
      } yield Fleet(owner, ships, planets(src), planets(dst), trip, turns)

  final val parser: Parser[Turn => GameState] =
    for {
      planets <- sepBy(planetParser, char('\n')).map(preparePlanets) <~ skipWhitespace
      fleets  <- sepBy(fleetParser(planets), char('\n')).map(_.toIndexedSeq)
    } yield (turn: Turn) => GameState(turn, planets, fleets)

  private def preparePlanets(planets: List[PlanetID => Planet]): IndexedSeq[Planet] =
    planets.zipWithIndex.map { case (p, i) => p(PlanetID(i)) }.toIndexedSeq
}
