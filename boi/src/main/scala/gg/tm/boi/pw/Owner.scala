package gg.tm.boi.pw

import cats.Eq
import monocle.Iso

sealed trait Owner extends Product with Serializable

object Owner {
  final case object Me      extends Owner
  final case object Neutral extends Owner
  final case object Enemy   extends Owner

  final val intIso: Iso[Owner, Int] = Iso[Owner, Int] {
    case Neutral => 0
    case Me      => 1
    case Enemy   => 2
  } {
    case 0 => Neutral
    case 1 => Me
    case 2 => Enemy
    case _ => sys.error("No.")
  }

  implicit val ownerEq: Eq[Owner] = Eq.fromUniversalEquals[Owner]
}
