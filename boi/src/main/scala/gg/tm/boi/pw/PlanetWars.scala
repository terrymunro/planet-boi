package gg.tm.boi.pw

import scala.io.StdIn

import atto.ParseResult
import cats.data.{ReaderT, State}
import cats.implicits._
import cats.kernel.Monoid

object PlanetWars {
  final val TurnDelimiter: String = "go"

  def start[S: Monoid](handleTurn: ReaderT[State[S, ?], GameState, List[Order]]): Unit =
    try {
      @SuppressWarnings(Array("org.wartremover.warts.Recursion"))
      def inner: State[(ParseResult[GameState], S), Unit] =
        for {
          (pr, s) <- State.get[(ParseResult[GameState], S)]
          line = StdIn.readLine()
          _ <- if (line === TurnDelimiter)
            pr.done.either match {
              case Right(gs1) =>
                val (ns, os) = handleTurn.run(gs1).run(s).value
                val gs2 = os.foldLeft(gs1)((s, o) => s.issue(o))
                println(TurnDelimiter)
                State.set((GameState.initialState(gs2.turn.incr), ns))
                  .flatMap(_ => inner)
              case Left(er) =>
                error(s"$er")
                sys.exit(1)
            }
          else State.set((pr.feed(line + "\n"), s)).flatMap(_ => inner)
      } yield ()

      inner.runA((GameState.initialState(Turn(1)), Monoid[S].empty)).value
    } catch { case ex: Throwable => error(s"I had a moment.\n$ex") }
}
