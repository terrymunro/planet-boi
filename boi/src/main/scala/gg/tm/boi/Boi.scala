package gg.tm.boi

import scala.annotation.tailrec

import cats.data.{ReaderT, State}
import cats.data.ReaderT.liftF
import cats.implicits._
import gg.tm.boi.Strats._
import gg.tm.boi.pw._


object Boi extends App {
  // Type aliases for convenience
  type F[A] = State[Journal, A]
  type RS[O] = ReaderT[F, GameState, O]

  /* Overrall phases my bot will go through to come up with its turns list of orders */
  def handleTurn: RS[List[Order]] =
    for {
      _  <- setupJournal
      _  <- defendMyTerritory
      _  <- verifyPlan
      os <- executePlan
    } yield os

  /* Any initial setup of an internally stored state */
  def setupJournal: RS[Unit] =
    for {
      j <- get
      _ <- setupInitialGraph.whenA(j.graph.isEmpty)
      _ <- setupInitialResources
    } yield ()

  /* Creates the initial graph and adds it to the journal (vertexes = planets, edges = distance) */
  def setupInitialGraph: RS[Unit] =
    for {
      gs <- ask
      _  <- modify(Journal.graph.set(Graph.init(gs.planets)))
    } yield ()

  /* Validate the plan is still okay and nothing opponent has done changes anything */
  def verifyPlan: RS[Unit] =
    for {
      gs <- ask
      p  <- inspect(_.plan)
      _  <- newPlan.whenA(p.isNoLongerValid(gs))
    } yield ()

  /* Converts plan into list of orders for this turn */
  def executePlan: RS[List[Order]] =
    for {
      gs <- ask
      p  <- inspect(_.plan)
//      _  <- set(nj)
    } yield Nil

  /* Setup unused resources (does this every turn to ensure its as accurate as possible) */
  def setupInitialResources: RS[Unit] =
    for {
      gs <- ask
      ts  = gs.myTotalShips
      inf = gs.myFleets.map(_.ships).sum
      unu = gs.myPlanets.map(p => p.id -> p.ships).toList
      _  <- modify(Journal.remaining.set(Resources(unu, Nil, inf, ts)))
    } yield ()

  /* Generates a brand new plan for when none exists */
  def newPlan: RS[Unit] =
    for {
      tgs <- findHighValueTargets
      ams <- tgs.traverse(createMoveFor)
      k = ams.toList
      _ = debug(s"KAAAY: $k")
      _   <- modify(moveLens.set(k))
    } yield ()

  /* Calculate required defence against any attacks on my territory */
  def defendMyTerritory: RS[List[Move]] =
    for {
      gs <- ask
      p  <- inspect(_.plan)
      ms <- gs.enemyFleets
        .filter(f => f.destination.isMine && f.mightCapture && !p.hasPlanFor(f))
        .toList
        .traverse(defendAgainst)
    } yield ms

  /* Create defensive move to handle a specific attack */
  def defendAgainst(fleet: Fleet): RS[Move] =
    for {
      gs  <- ask
      g   <- inspect(_.graph)
      t = fleet.destination
      s = math.max(0, fleet.ships - t.ships - (t.growthRate * fleet.turnsRemaining))
      (ss, _) = g.findAllWithin(t.id, Distance(fleet.turnsRemaining)).view
        .map(gs.planets)
        .filter(_.isMine)
        .sortBy(p => g.distance(p.id, t.id))
        .takeWhile(_.ships < s)
        .foldLeft((List.empty[Future[Step]], 0)) { case (a@(acc, r), p) =>
          val time = fleet.turnsRemaining - p.distanceTo(t) - 1
          val n = Ships(math.min(p.ships + (p.growthRate * time), s-r))
          if (n <= 0) a
          else // TODO Merge steps on same turn
            ( Future[Step](
              intTurn.get(time),
              Step(Owner.Me, t, List(Order(p.id, t.id, n)))) :: acc
            , r - n
            )
        }
      _ <- ss.filter(_.turn === gs.turn).traverse(reserveShips)
    } yield Move(Owner.Me, Left(fleet), ss)

  def reserveShips(fs: Future[Step]): RS[Unit] = {
    val o :: _ = fs.t.orders
    for {
      gs <- ask
      _  <- modify(Journal.remaining.modify(_.use(o.ships).from(gs.planets(o.srcId))))
    } yield ()
  }

  /* Find targets that are low risk and or high reward */
  def findHighValueTargets: RS[IndexedStream[Planet]] =
    for {
      gs     <- ask
      unused <- inspect(unusedCount.getAll)
    } yield gs.possibleTargets(unused.sum)

  /* Create a move for capturing a planet */
  def createMoveFor(target: Planet): RS[Move] = {
    @tailrec
    def inner(planets: Stream[Planet], r: Int, fs: List[Step]): List[Step] =
      if (planets.isEmpty || r <= 0) fs
      else {
        val p #:: tail = planets
        val n = math.max(r - p.ships, p.ships - r)
        val os = List(Order(p.id, target.id, Ships(n)))
        debug(s"r: $r  n: $n  p: $p")
        inner(tail, r-n, Step(Owner.Me, target, os) :: fs)
      }

    for {
      gs <- ask
      gr <- inspect(_.graph)
      ps = gs.myPlanets.sortBy(p => gr.distance(p.id, target.id)).toStream
      steps = inner(ps, target.ships+1, List.empty[Step])
    } yield Move(Owner.Me, Right(target), steps.map(Future(gs.turn, _)))
  }

  //  ((my, enemy), dist) = j.graph.findClosestPair(gs.myPlanets.map(_.id), gs.enemyPlanets.map(_.id))
  //  (mp, ep) = (gs.planets(my), gs.planets(enemy))
  //  res = ts - inf - (ep.ships - (mp.growthRate * dist))
  //        x = j.graph.findAllWithin(my, dist).view.map(gs.planets).filter(_.isMine).force

  /* Find the planet that I consider the most "strategically" valuable on this turn */
  def mostStrategicallyValuable: RS[Option[Planet]] =
    for {
      gs <- ask
      op <- inspect(_.graph.foldLeft((Option.empty[Planet], Distance(0))) {
        case (o@(p, min), (ds, i)) =>
          val sum = ds.sum
          val pp = gs.planets(i)
          if (sum === min) {
            val myC = gs.myCenter
            p.filter(_.location.distanceTo(myC) < pp.location.distanceTo(myC))
              .orElse(Some(pp)) -> sum
          } else if (sum < min) Some(pp) -> sum
          else o
      }._1)
    } yield op

  /* Monad Transformer helpers */
  def ask: RS[GameState] = ReaderT.ask[F, GameState]
  def get: RS[Journal] = liftF(State.get[Journal])
  def set(nj: Journal): RS[Unit] = liftF[F, GameState, Unit](State.set[Journal](nj))
  def inspect[R](f: Journal => R): RS[R] = liftF(State.inspect[Journal, R](f))
  def modify(f: Journal => Journal): RS[Unit] = liftF(State.modify[Journal](f))

  /* Start the bot :p */
  PlanetWars.start(handleTurn)
}
