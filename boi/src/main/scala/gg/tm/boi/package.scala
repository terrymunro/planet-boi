package gg.tm

import cats.{Monoid, MonoidK}
import cats.implicits._
import mouse.boolean._
import gg.tm.boi.Strats._
import gg.tm.boi.pw.{Order, Owner, PlanetID, Ships, Turn}
import monocle.{Lens, Prism, Traversal}
import monocle.function.fields._

package object boi {
  def const[A, B]: A => B => A = Function.const[A, B]

  val moveTraversal: Traversal[List[Move], Move] = Traversal.fromTraverse[List, Move]
  val moveLens: Lens[Journal, List[Move]] = Journal.plan.composeLens(Plan.moves)
  val reservedLens: Lens[Journal, List[(PlanetID, Ships)]] = Journal.remaining.composeLens(Resources.reserved)
  val unusedLens: Lens[Journal, List[(PlanetID, Ships)]] = Journal.remaining.composeLens(Resources.unused)
  val unusedCount: Traversal[Journal, Ships] = unusedLens.composeTraversal(Traversal.fromTraverse[List, (PlanetID, Ships)]).composeLens(second)
  def stepsTraversal: Traversal[Plan, List[Future[Step]]] =
    Plan.moves
      .composeTraversal(moveTraversal)
      .composePrism(moveOwnerPrism(Owner.Me))
      .composeLens(Move.steps)

  def moveOwnerPrism(owner: Owner): Prism[Move, Move] =
    Prism[Move, Move](m => (m.owner === owner).option(m))(identity)

  def currentStepTraversal(turn: Turn): Traversal[List[Future[Step]], Step] =
    Traversal.fromTraverse[List, Future[Step]].composePrism(presentStepPrism(turn))

  def presentStepPrism(turn: Turn): Prism[Future[Step], Step] =
    Prism[Future[Step], Step](fs => (fs.turn === turn).option(fs.t))(Future(turn, _))

  def getTurnOrders(turn: Turn)(p: Plan): List[Order] =
    stepsTraversal
      .composeTraversal(currentStepTraversal(turn))
      .composeLens(Step.orders)
      .getAll(p)
      .flatten

  /* For convenience */
  def M[A](implicit m: Monoid[A]): Monoid[A] = m

  def listMerge: MonoidK[List] = new MonoidK[List] {
    def empty[A]: List[A] = Nil
    def combineK[A](a: List[A], b: List[A]): List[A] =
      a ++ (a diff b)
  }

  implicit class EquivOps[T](private val self: T) extends AnyVal {
    def equiv(that: T)(implicit eq: Equiv[T]): Boolean = eq.equiv(self, that)
  }
}
