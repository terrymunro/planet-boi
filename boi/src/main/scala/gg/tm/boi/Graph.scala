package gg.tm.boi

import gg.tm.boi.pw.{Distance, Planet, PlanetID}

/**
  * Graph is an undirected weighted graph where weights are specifically distances in this program
  *
  * @param underlying Matrix of distances between Planets
  */
final class Graph private(private val underlying: IndexedSeq[IndexedSeq[Distance]]) extends AnyVal {
  def isEmpty: Boolean  = underlying.isEmpty
  def nonEmpty: Boolean = !isEmpty

  def distance(a: PlanetID, b: PlanetID): Distance =
    underlying(a)(b)

  def findAllWithin(target: PlanetID, dist: Distance): Seq[PlanetID] =
    underlying(target)
      .view
      .zipWithIndex
      .filter { case (d, _) => d < dist }
      .map { case (_, i) => PlanetID(i) }

  def foldLeft[A](z: A)(f: (A, (IndexedSeq[Distance], Int)) => A): A =
    underlying
      .view
      .zipWithIndex
      .foldLeft(z)(f)

  def findClosestPair(mine: Seq[PlanetID], enemies: Seq[PlanetID]): ((PlanetID, PlanetID), Distance) =
    (for { m <- mine; e <- enemies } yield (m, e))
      .foldLeft(((PlanetID(-1), PlanetID(-1)), Distance(999))) { case (c@(_, min), (m, e)) =>
        val d = underlying(m)(e)
        if (d < min) ((m, e), d) else c
      }

  // TODO: Minimum spanning tree
  def mst = ???
}

object Graph {
  /**
    * Initialize Graph
    *
    * @param planets All the planets for this run
    * @return Graph
    */
  def init(planets: IndexedSeq[Planet]): Graph =
    new Graph(planets.map(a => planets.map(b => a.distanceTo(b))))

  def empty: Graph = init(IndexedSeq.empty)
}
