package gg.tm.boi

import cats.{Functor, Monoid}
import cats.data.Reader
import cats.implicits._
import cats.kernel.PartialOrder
import gg.tm.boi.pw._
import monocle.{Prism, Traversal}
import monocle.macros.Lenses
import mouse.all._

object Strats {
  /* Resources are some running counts of ships that are available / in use */
  @Lenses final case class Resources(unused: List[(PlanetID, Ships)], reserved: List[(PlanetID, Ships)], inFlight: Ships, total: Ships) {
    def use(s: Ships): Resources.UsePartiallyApped = new Resources.UsePartiallyApped(s, this)
  }
  object Resources {
    val unusedTraversal: Traversal[List[(PlanetID, Ships)], (PlanetID, Ships)] =
      Traversal.fromTraverse[List, (PlanetID, Ships)]
    def unusedPrism(p: Planet): Prism[(PlanetID, Ships), (PlanetID, Ships)] =
      Prism[(PlanetID, Ships), (PlanetID, Ships)](r => (r._1 === p.id).option(r))(identity)
    def resourceUnusedFor(p: Planet): Traversal[Resources, (PlanetID, Ships)] =
      unused.composeTraversal(unusedTraversal).composePrism(unusedPrism(p))

    final class UsePartiallyApped(private val s: Ships, private val self: Resources) {
      def from(p: Planet): Resources =
        reserved.modify((p.id -> s) :: _)(
          resourceUnusedFor(p).modify(t => t.copy(_2 = Ships(t._2 - s)))(self))
    }

    implicit val resourcesMonoid: Monoid[Resources] = new Monoid[Resources] {
      override def empty: Resources = Resources(Nil, Nil, Ships(0), Ships(0))
      override def combine(x: Resources, y: Resources): Resources =
        if      (x.total === Ships(0) || x.total > y.total) y
        else if (y.total === Ships(0) || x.total < y.total) x
        else (x, y) match {
          case (Resources(xu, xr, xi, t), Resources(yu, yr, yi, _)) =>
            val inFlight = Ships(xi.max(yi))
            val reserved = listMerge.combineK(xr,yr)
            val unused = listMerge.combineK(xu, yu)
            Resources(unused, reserved, inFlight, t)
        }
    }
  }

  /* A step is a single turns list of orders for a specific destination planet */
  @Lenses final case class Step(owner: Owner, dst: Planet, orders: List[Order]) {
    def isMe: Boolean = owner === Owner.Me
    def isEmpty: Boolean = orders.isEmpty
    def nonEmpty: Boolean = !isEmpty
  }

  /* A move is a list of future steps (where current steps are converted to orders) */
  @Lenses final case class Move(owner: Owner, target: Either[Fleet, Planet], steps: List[Future[Step]]) {
    def isMine: Boolean = owner === Owner.Me
    def isEmpty: Boolean = steps.isEmpty
    def nonEmpty: Boolean = !isEmpty
    def ordersForTurn(t: Turn): (Move, List[Order]) = {
      val v = steps.view
      val (a, b) = v.sortBy(_.turn).splitAt(v.lastIndexWhere(_.turn === t))
      debug(s"A: $a  B: $b")
      (copy(steps = b.toList), a.flatMap(_.t.orders).toList)
    }
  }

  /* My current plan is a list of moves */
  @Lenses final case class Plan private(moves: List[Move]) {
    def isEmpty: Boolean = moves.isEmpty
    def nonEmpty: Boolean = !isEmpty

    def hasPlanFor(f: Fleet): Boolean =
      moves.exists(m => m.target.fold(_.equiv(f), const(false)))

    // TODO: Finish
    def isNoLongerValid: Reader[GameState, Boolean] =
      Reader(_ => isEmpty)

    def extractOrders(t: Turn): (Plan, List[Order]) = {
      // TODO: Use lenses maybe to update plan maybe. i dunno..
      val mvs = moves.filter(_.steps.exists(_.turn === t))
      ???
    }
  }
  object Plan {
    implicit val planMonoid: Monoid[Plan] = new Monoid[Plan] {
      override def empty: Plan = Plan(Nil)
      override def combine(x: Plan, y: Plan): Plan =
        Plan(listMerge.combineK(x.moves, y.moves))
    }
  }

  /* this is my internal state type */
  @Lenses final case class Journal(graph: Graph, plan: Plan, remaining: Resources)
  object Journal {
    implicit val journalMonoid: Monoid[Journal] = new Monoid[Journal] {
      override def empty: Journal = Journal(Graph.empty, M[Plan].empty, M[Resources].empty)
      override def combine(x: Journal, y: Journal): Journal =
        Journal(
          x.graph,
          x.plan |+| y.plan,
          x.remaining |+| y.remaining
        )
    }
  }

  /* Models future things.. steps/orders/moves etc */
  final case class Future[T](turn: Turn, t: T) {
    def map[R](f: T => R): Future[R] = copy(t = f(t))
  }
  object Future {
    implicit def futurePartialOrder[T]: PartialOrder[Future[T]] =
      PartialOrder.from(_.turn compare _.turn)

    implicit val futureFunctor: Functor[Future] = new Functor[Future] {
      override def map[A, B](fa: Future[A])(f: A => B): Future[B] =
        fa.map(f)
    }
  }
}
