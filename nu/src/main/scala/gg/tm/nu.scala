package gg.tm

import scala.language.experimental.macros
import scala.annotation.StaticAnnotation
import scala.annotation.compileTimeOnly
import scala.reflect.macros.whitebox

@compileTimeOnly("Enable macro paradise to expand 𝜈 macro annotations")
class nu extends StaticAnnotation {
  def macroTransform(annottees: Any*): Any = macro nu.impl
}

object nu {
  sealed trait Nu[U]
  type @@[+T, U] = T with Nu[U]

  def impl(c: whitebox.Context)(annottees: c.Expr[Any]*): c.Expr[Any] = {
    import c.universe._
//    @SuppressWarnings(Array("org.wartremover.warts.AsInstanceOf"))
    val result = annottees.map(_.tree).toList match {
      case q"$mods type $tname[..$typeParams] = $tpt" :: Nil =>
        val rhs: TypeName = TypeName(tpt.toString)
        val typeName: TypeName = tname
        val tagName: TypeName = TypeName(s"${typeName}Type")
        val termName: TermName = TermName(tname.toString)
        q"""$mods type $typeName[..$typeParams] = gg.tm.nu.@@[$rhs,$termName.$tagName]
            $mods object $termName {
              sealed trait $tagName
              def apply(a: $rhs): $typeName = a.asInstanceOf[gg.tm.nu.@@[$rhs,$tagName]]
              def unapply(t: $typeName): Option[$rhs] = Some(t)
            }
         """
      case _ => c.abort(c.enclosingPosition, "@nu only works on type definitions")
    }
    c.Expr[Any](result)
  }
}
