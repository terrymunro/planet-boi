package gg.tm.schwi

import pw.reee._

object Schwi extends App {
  def handleTurn(gs: GameState): List[Order] =
    try {
      val myFleets = gs.fleets.filter(_.owner == Owner.Me)
      val (myPlanets, conquest) = gs.planets.partition(_.owner == Owner.Me)
      val remainingConquest = conquest.filterNot(p =>
        myFleets.exists(_.destination.id == p.id))
      if (myFleets.length < 5 && remainingConquest.nonEmpty) {
        val src = myPlanets.maxBy(_.ships)
        val dst = remainingConquest.minBy(p => Math.abs(p.ships + (Point.distance(p.location, src.location) * p.growthRate)))
        val o = Order(src.id, dst.id, Math.min(1 + dst.ships + Point.distance(dst.location, src.location) * dst.growthRate, src.ships))
        List(o)
      } else Nil
    } catch {
      case t: Throwable =>
        error(s"Reeee: ${t.getMessage}")
        Nil
    }

  PlanetWars.start(handleTurn)
}


